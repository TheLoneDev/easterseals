'use strict';

angular.module('volunteerApp')
    .controller('NavbarCtrl', function($scope, $location, Auth) {
        $scope.menu = [{
            'title': 'Home',
            'link': '/'
        }, {
            'title': 'Hour Log',
            'link': '/hours'
        }, {
            'title': 'Volunteers',
            'link': '/volunteers'
        }, {
            'title': 'Locations',
            'link': '/locations'
        }, {
            'title': 'Interests',
            'link': '/interests'
        }, {
            'title': 'Skills',
            'link': '/skills'
        }];

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;
        $scope.getCurrentUser = Auth.getCurrentUser;

        $scope.logout = function() {
            Auth.logout();
            $location.path('/login');
        };

        $scope.isActive = function(route) {
            return route === $location.path();
        };
    });
