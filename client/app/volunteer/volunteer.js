'use strict';

angular.module('volunteerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('new volunteer', {
        url: '/volunteer/new',
        templateUrl: 'app/volunteer/new.html',
        controller: 'VolunteerCtrl'
      }).state('edit volunteer', {
        url: '/volunteer/:volunteerId/edit',
        templateUrl: 'app/volunteer/edit.html',
        controller: 'VolunteerCtrl'
      }).state('volunteer view', {
        url: '/volunteer/:volunteerId',
        templateUrl: 'app/volunteer/view.html',
        controller: 'VolunteerCtrl'
      }).state('volunteers', {
        url: '/volunteers',
        templateUrl: 'app/volunteer/list.html',
        controller: 'VolunteerCtrl'
      });
  });
