'use strict';

angular.module('volunteerApp')
    .factory('Volunteer', ['$resource',
        function($resource) {
            return $resource('/api/volunteers/:volunteerId', {
                volunteerId: '@_id'
            }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
