'use strict';

angular.module('volunteerApp')
    .controller('VolunteerCtrl', ['$scope', 'Volunteer', 'Skill', 'Locations', 'Interest', '$location', '$stateParams', '$http',
        function($scope, Volunteer, Skill, Locations, Interest, $location, $stateParams, $http) {

            $scope.volunteers = [];

            $scope.areasOfInterest = [];

            $scope.specialSkills = [];

            $scope.locations = [];

            $scope.volunteer = {
                status: '',
                firstName: '',
                middleName: '',
                lastName: '',
                address1: '',
                address2: '',
                city: '',
                state: '',
                zip: '',
                email: '',
                phoneHome: '',
                phoneCell: '',
                ssn: '',
                dob: '',
                school: '',
                degree: '',
                employer: '',
                jobTitle: '',
                specialSkills: [],
                locations: [],
                areasOfInterest: [],
                availability: {
                    monday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    },
                    tuesday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    },
                    wednesday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    },
                    thursday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    },
                    friday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    },
                    saturday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    },
                    sunday: {
                        morning: false,
                        afternoon: false,
                        evening: false
                    }

                }

            };

            $scope.createVolunteer = function() {
                alert('Creating Volunteer!');
                for (var i in $scope.areasOfInterest) {
                    if ($scope.areasOfInterest[i].isChecked) {
                        $scope.volunteer.areasOfInterest.push($scope.areasOfInterest[i].name)
                    }
                }
                for (var i in $scope.locations) {
                    if ($scope.locations[i].isChecked) {
                        $scope.volunteer.locations.push($scope.locations[i].name)
                    }
                }
                for (var i in $scope.specialSkills) {
                    if ($scope.specialSkills[i].isChecked) {
                        $scope.volunteer.specialSkills.push($scope.specialSkills[i].name)
                    }
                }

                var vol = new Volunteer($scope.volunteer);
                vol.$save(function(response) {
                    alert('Yay!');
                    $location.path('volunteer/' + response._id);
                });
            };

            $scope.updateVolunteer = function() {
                for (var i in $scope.areasOfInterest) {
                    if ($scope.areasOfInterest[i].isChecked) {
                        $scope.volunteer.areasOfInterest.push($scope.areasOfInterest[i].name)
                    }
                }
                for (var i in $scope.locations) {
                    if ($scope.locations[i].isChecked) {
                        $scope.volunteer.locations.push($scope.locations[i].name)
                    }
                }
                for (var i in $scope.specialSkills) {
                    if ($scope.specialSkills[i].isChecked) {
                        $scope.volunteer.specialSkills.push($scope.specialSkills[i].name)
                    }
                }

                var vol = $scope.volunteer;
                vol.$update(function() {
                    $location.path('volunteer/' + vol._id);
                });
            };

            $scope.find = function() {
                Volunteer.query(function(volunteers) {
                    $scope.volunteers = volunteers;
                });
            };

            $scope.findOne = function() {
                Volunteer.get({
                    volunteerId: $stateParams.volunteerId
                }, function(volunteer) {
                    $scope.volunteer = volunteer;
                    console.log(volunteer);
                    $scope.checkBoxes();
                });



            };

            $scope.checkBoxes = function() {
                for (var i in $scope.volunteer.areasOfInterest) {
                    var interest = $scope.volunteer.areasOfInterest[i];
                    for (var j in $scope.areasOfInterest) {
                        if ($scope.areasOfInterest[j].name == interest) {
                            $scope.areasOfInterest[j].isChecked = true;
                        }
                    }
                }
                for (var i in $scope.volunteer.locations) {
                    var location = $scope.volunteer.locations[i];
                    for (var j in $scope.locations) {
                        if ($scope.locations[j].name == location) {
                            $scope.locations[j].isChecked = true;
                        }
                    }
                }
                for (var i in $scope.volunteer.specialSkills) {
                    var skill = $scope.volunteer.specialSkills[i];
                    for (var j in $scope.specialSkills) {
                        if ($scope.specialSkills[j].name == skill) {
                            $scope.specialSkills[j].isChecked = true;
                        }
                    }
                }
            }

            $scope.delete = function(volunteer) {
                volunteer.$remove(function(response) {
                    for (var i in $scope.volunteers) {
                        if ($scope.volunteers[i] === volunteer) {
                            $scope.volunteers.splice(i, 1);
                        }
                    }
                });
            };

            $scope.loadInterestsLocationsSkills = function() {
                Locations.query(function(locations) {
                    $scope.locations = locations;
                    //$scope.checkBoxes();
                });
                Skill.query(function(specialSkills) {
                    $scope.specialSkills = specialSkills;
                    //$scope.checkBoxes();
                });
                Interest.query(function(areasOfInterest) {
                    $scope.areasOfInterest = areasOfInterest;
                    //$scope.checkBoxes();
                });
            };


            $scope.getLogs = function() {
                $http.get('/api/hour/volunteer/' + $stateParams.volunteerId).
                success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.hourLogs = data;

                }).
                error(function(data, status, headers, config) {

                });
            };

            //Returns hours worked in minutes
            $scope.hoursWorked = function(i, o) {
            var date1;
            var date2;
            var splitted = i.split(' ')[0].split(':');
            if (i.toLowerCase().indexOf('pm') > -1) {
                date1 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
            } else {
                date1 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
            }
            splitted = o.split(' ')[0].split(':');
            if (o.toLowerCase().indexOf('pm') > -1) {
                date2 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
            } else {
                date2 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
            }
            var diff = Math.abs(date2.getTime() - date1.getTime());
            var mm = diff / 60000;
            return mm;
        };

        $scope.hoursToString = function(hours) {
            var hh = parseInt(hours / 60);
            var mm = parseInt(hours % 60);
            return hh + ' hours and ' + mm + ' minutes';
        }

        }
    ]);
