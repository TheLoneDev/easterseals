'use strict';

describe('Controller: HoursCtrl', function () {

  // load the controller's module
  beforeEach(module('volunteerApp'));

  var HoursCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HoursCtrl = $controller('HoursCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
