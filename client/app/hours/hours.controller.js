'use strict';

angular.module('volunteerApp')
    .controller('HoursCtrl', ['$scope', 'Hour', 'Volunteer', 'Locations', function($scope, Hour, Volunteer, Locations) {
        $scope.logs = [{
            volunteer: {
                first: 'Tim',
                last: 'Christensen'
            },
            date: '4-20-2015',
            timeIn: '3:30 PM',
            timeOut: '8:30 PM'
        }];

        $scope.hourLog = {
            volunteer: '',
            date: '',
            timeIn: '',
            timeOut: '',
            location: ''
        };


        $scope.loadVolunteers = function() {
            Volunteer.query(function(volunteers) {
                $scope.volunteers = volunteers;
            });
        };
        $scope.loadLocations = function() {
            Locations.query(function(locations) {
                $scope.locations = locations;
                $scope.hourLog.location = $scope.locations[0];
            });
        }

        $scope.selectVolunteer = function(volunteer) {
            $scope.hourLog.volunteer = volunteer;

        }

        $scope.delete = function(log) {
            log.$remove(function(response) {
                for (var i in $scope.logs) {
                    if ($scope.logs[i] === log) {
                        $scope.logs.splice(i, 1);
                    }
                }
            });
        };




        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.dt = null;
        };


        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];


        $scope.create = function() {
            $scope.errorMsg = '';
            if ($scope.hourLog.timeIn.toLowerCase().indexOf('am') == -1 && $scope.hourLog.timeIn.toLowerCase().indexOf('pm') == -1) {
                $scope.errorMsg = $scope.hourLog.timeIn + ' is not a valid time.';
            }
            if ($scope.hourLog.timeOut.toLowerCase().indexOf('am') == -1 && $scope.hourLog.timeOut.toLowerCase().indexOf('pm') == -1) {
                $scope.errorMsg = $scope.hourLog.timeOut + ' is not a valid time.';
            }
            if ($scope.errorMsg === '') {
                $scope.hourLog.location = $scope.hourLog.location.name;
                var o = new Hour($scope.hourLog);
                o.$save(function(response) {
                    o.volunteer = $scope.hourLog.volunteer;
                    $scope.find();
                });
            }

        };
        $scope.find = function() {
            Hour.query(function(hours) {
                $scope.logs = hours;
            });
        };

        $scope.hoursWorked = function(i, o) {
            var date1;
            var date2;
            var splitted = i.split(' ')[0].split(':');
            if (i.toLowerCase().indexOf('pm') > -1) {
                date1 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
            } else {
                date1 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
            }
            splitted = o.split(' ')[0].split(':');
            if (o.toLowerCase().indexOf('pm') > -1) {
                date2 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
            } else {
                date2 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
            }
            var diff = Math.abs(date2.getTime() - date1.getTime());
            var mm = diff / 60000;
            return mm;
        };

        $scope.hoursToString = function(hours) {
            var hh = parseInt(hours / 60);
            var mm = parseInt(hours % 60);
            return hh + ' hours and ' + mm + ' minutes';
        }


    }]);
