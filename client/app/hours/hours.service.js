'use strict';

angular.module('volunteerApp')
    .factory('Hour', ['$resource',
        function($resource) {
            return $resource('/api/hour/:hoursId', {
                hoursId: '@_id'
            }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
