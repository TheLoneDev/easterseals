'use strict';

describe('Service: hours', function () {

  // load the service's module
  beforeEach(module('volunteerApp'));

  // instantiate service
  var hours;
  beforeEach(inject(function (_hours_) {
    hours = _hours_;
  }));

  it('should do something', function () {
    expect(!!hours).toBe(true);
  });

});
