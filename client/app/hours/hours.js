'use strict';

angular.module('volunteerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('hours', {
        url: '/hours',
        templateUrl: 'app/hours/hours.html',
        controller: 'HoursCtrl'
      });
  });