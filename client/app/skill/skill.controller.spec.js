'use strict';

describe('Controller: SkillCtrl', function () {

  // load the controller's module
  beforeEach(module('volunteerApp'));

  var SkillCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SkillCtrl = $controller('SkillCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
