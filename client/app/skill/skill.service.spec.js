'use strict';

describe('Service: skill', function () {

  // load the service's module
  beforeEach(module('volunteerApp'));

  // instantiate service
  var skill;
  beforeEach(inject(function (_skill_) {
    skill = _skill_;
  }));

  it('should do something', function () {
    expect(!!skill).toBe(true);
  });

});
