'use strict';

angular.module('volunteerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('skill', {
        url: '/skills',
        templateUrl: 'app/skill/skill.html',
        controller: 'SkillCtrl'
      });
  });
