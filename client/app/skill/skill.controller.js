'use strict';

angular.module('volunteerApp')
    .controller('SkillCtrl', ['$scope', 'Skill', '$location', '$stateParams',
        function($scope, Skill, $location, $stateParams) {

            $scope.skill = {
                name: ''
            };

            $scope.skills = [];

            $scope.create = function() {

                var vol = new Skill($scope.skill);
                vol.$save(function(response) {
                    $scope.skills.push(vol);
                    $scope.skill.name = '';
                });
            };

            $scope.update = function(skill) {
                console.log(skill);
                skill.$update(function() {

                });
            };

            $scope.find = function() {
                Skill.query(function(skills) {
                    $scope.skills = skills;
                });
            };


            $scope.delete = function(skill) {
                skill.$remove(function(response) {
                    for (var i in $scope.skills) {
                        if ($scope.skills[i] === skill) {
                            $scope.skills.splice(i, 1);
                        }
                    }
                });
            };

        }
    ]);
