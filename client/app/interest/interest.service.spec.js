'use strict';

describe('Service: interest', function () {

  // load the service's module
  beforeEach(module('volunteerApp'));

  // instantiate service
  var interest;
  beforeEach(inject(function (_interest_) {
    interest = _interest_;
  }));

  it('should do something', function () {
    expect(!!interest).toBe(true);
  });

});
