'use strict';

angular.module('volunteerApp')
    .controller('InterestCtrl', ['$scope', 'Interest', '$location', '$stateParams',
        function($scope, Interest, $location, $stateParams) {

            $scope.interest = {
                name: ''
            };

            $scope.interests = [];

            $scope.create = function() {

                var o = new Interest($scope.interest);
                o.$save(function(response) {
                    $scope.interests.push(o);
                    $scope.interest.name = '';
                });
            };

            $scope.update = function(interest) {

                interest.$update(function() {
                    $location.path('interest/' + vol._id);
                });
            };

            $scope.find = function() {
                Interest.query(function(interests) {
                    $scope.interests = interests;
                });
            };

            $scope.findOne = function() {
                Interest.get({
                    interestId: $stateParams.interestId
                }, function(interest) {
                    $scope.interest = interest;
                });
            };

            $scope.delete = function(interest) {
                interest.$remove(function(response) {
                    for (var i in $scope.interests) {
                        if ($scope.interests[i] === interest) {
                            $scope.interests.splice(i, 1);
                        }
                    }
                });
            };

        }
    ]);
