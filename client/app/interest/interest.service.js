'use strict';

angular.module('volunteerApp')
    .factory('Interest', ['$resource',
        function($resource) {
            return $resource('/api/interests/:interestId', {
                interestId: '@_id'
            }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
