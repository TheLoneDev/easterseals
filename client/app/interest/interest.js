'use strict';

angular.module('volunteerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('interest', {
        url: '/interests',
        templateUrl: 'app/interest/interest.html',
        controller: 'InterestCtrl'
      });
  });
