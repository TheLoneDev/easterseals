'use strict';

angular.module('volunteerApp')
    .controller('MainCtrl', function($scope, $http, Hour) {

        $scope.totalWorked = 0;
        $scope.endDate = new Date();
        $scope.startDate = new Date($scope.endDate.getFullYear(), 0);

        $scope.report = {
            start: '',
            end: '',
            department: ''
        };

        $scope.hoursWorked = function(i, o) {
            var date1;
            var date2;
            var splitted = i.split(' ')[0].split(':');
            if (i.toLowerCase().indexOf('pm') > -1) {
                date1 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
            } else {
                date1 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
            }
            splitted = o.split(' ')[0].split(':');
            if (o.toLowerCase().indexOf('pm') > -1) {
                date2 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
            } else {
                date2 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
            }
            var diff = Math.abs(date2.getTime() - date1.getTime());
            var mm = diff / 60000;
            return mm;
        };

        $scope.hoursToString = function(hours) {
            var hh = parseInt(hours / 60);
            var mm = parseInt(hours % 60);
            return hh + ' hours and ' + mm + ' minutes';
        }
        $scope.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        $scope.labels = [];

        $scope.data = [
            []
        ];
        $scope.hoursLoggedData = [
            []
        ];

        $scope.lineLabels = [];
        $scope.series = ['New Volunteers'];
        $scope.lineData = [
            []
        ];
        $scope.generateReport = function() {
            if ($scope.report.start !== '') {
                $scope.startDate = new Date($scope.report.start);
                $scope.endDate = new Date($scope.report.end);
            }

            $scope.hoursLoggedData[0] = [];
            $scope.totalWorked = 0;
            if ($scope.report.department !== '') {
                for (var i = $scope.startDate.getMonth() + 1; i < $scope.endDate.getMonth() + 2; i++) {
                    $http.get('/api/hour/department/' + $scope.report.department + '/count/' + i + '/' + $scope.endDate.getFullYear()).
                    success(function(data, status, headers, config) {
                        console.log(data);
                        $scope.hoursLoggedData[0][data.month - 1 - $scope.startDate.getMonth()] = data.hours / 60; //.push(data.hours / 60); // [data.month - 1] = data.hours / 60;
                        $scope.totalWorked += data.hours;
                    }).
                    error(function(data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
                }
            } else {
                for (var i = $scope.startDate.getMonth() + 1; i < $scope.endDate.getMonth() + 2; i++) {
                    $http.get('/api/hour/hours/' + i + '/' + $scope.endDate.getFullYear()).
                    success(function(data, status, headers, config) {
                        $scope.hoursLoggedData[0][data.month - 1 - $scope.startDate.getMonth()] = data.hours / 60; //.push(data.hours / 60); // [data.month - 1] = data.hours / 60;
                        $scope.totalWorked += data.hours;
                    }).
                    error(function(data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
                }

            }
            $scope.data[0] = [];

            for (var i = $scope.startDate.getMonth() + 1; i < $scope.endDate.getMonth() + 2; i++) {
                $http.get('/api/hour/count/' + i + '/' + $scope.endDate.getFullYear()).
                success(function(data, status, headers, config) {
                    $scope.data[0][data.month - 1 - $scope.startDate.getMonth()] = data.logs; //[data.month - 1] = data.logs;

                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            }
            $scope.labels = [];
            for (var i = $scope.startDate.getMonth(); i < $scope.endDate.getMonth() + 1; i++) {
                $scope.labels.push($scope.months[i]);
            }

            $scope.lineData[0] = [];
            for (var i = $scope.startDate.getMonth() + 1; i < $scope.endDate.getMonth() + 2; i++) {
                $http.get('/api/volunteers/count/' + i + '/' + $scope.endDate.getFullYear()).
                success(function(data, status, headers, config) {
                    $scope.lineData[0][data.month - 1 - $scope.startDate.getMonth()] = data.volunteers; //[(data.month - 1)] = data.volunteers;

                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });
            }
            $scope.lineLabels = [];
            for (var i = $scope.startDate.getMonth(); i < $scope.endDate.getMonth() + 1; i++) {
                $scope.lineLabels.push($scope.months[i]);
            }
        };



        $scope.getTotalVolunteers = function() {
            $http.get('/api/volunteers/count/').
            success(function(data, status, headers, config) {
                $scope.totalVolunteers = data.volunteers;

            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        };



        $scope.clear = function() {
            $scope.dt = null;
        };


        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

    });
