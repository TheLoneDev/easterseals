'use strict';

angular.module('volunteerApp')
    .controller('LocationCtrl', ['$scope', 'Locations', '$location', '$stateParams',
        function($scope, Locations, $location, $stateParams) {

            $scope.location = {
                name: ''
            };
            $scope.loactions = [];

            $scope.create = function() {
                console.log($scope.location);
                var vol = new Locations($scope.location);
                vol.$save(function(response) {
                    $scope.locations.push(vol);
                });
                $scope.location.name = '';
            };

            $scope.update = function(location) {

                location.$update(function() {

                });
            };

            $scope.find = function() {
                Locations.query(function(locations) {
                    $scope.locations = locations;
                });
            };

            $scope.delete = function(location) {
                location.$remove(function(response) {
                    for (var i in $scope.locations) {
                        if ($scope.locations[i] === location) {
                            $scope.locations.splice(i, 1);
                        }
                    }
                });
            };
        }
    ]);
