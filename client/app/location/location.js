'use strict';

angular.module('volunteerApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('locations', {
        url: '/locations',
        templateUrl: 'app/location/list.html',
        controller: 'LocationCtrl'
      });
  });
