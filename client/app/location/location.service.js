'use strict';

angular.module('volunteerApp')
    .factory('Locations', ['$resource',
        function($resource) {
            return $resource('/api/locations/:locationId', {
                locationId: '@_id'
            }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
