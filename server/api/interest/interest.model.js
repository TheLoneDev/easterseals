'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var InterestSchema = new Schema({
  name: String
});

module.exports = mongoose.model('Interest', InterestSchema);
