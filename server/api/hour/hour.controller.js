'use strict';

var _ = require('lodash');
var Hours = require('./hour.model');
var Volunteer = require('../volunteer/volunteer.model');

// Get list of hours
exports.index = function(req, res) {
    Hours.find().sort({
        date: -1
    }).populate('volunteer').exec(function(err, hours) {
        if (err) {
            return res.status(500).json({
                error: 'Cannot list Hour Logs'
            });
        }

        return res.json(200, hours);
    })
};

// Get a single hours
exports.show = function(req, res) {
    Hours.findById(req.params.id, function(err, hours) {
        if (err) {
            return handleError(res, err);
        }
        if (!hours) {
            return res.send(404);
        }
        return res.json(hours);
    });
};

// Creates a new hours in the DB.
exports.create = function(req, res) {
    Volunteer.findById(req.body.volunteer._id, function(err, volunteer) {
        req.body.volunteer = volunteer._id;
        var hours = new Hours(req.body);
        hours.save(function(err) {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    error: 'Cannot create the Hour Log'
                });
            }
            res.json(hours);
        })
    });
    /*Hours.create(req.body, function(err, hours) {
        if (err) {
            console.log(err);
            return handleError(res, err);
        }
        return res.json(201, hours);
    });*/
};

exports.findByVolunteer = function(req, res) {
    console.log('Finding by ID: ' + req.params.id);
    Hours.find({
        volunteer: req.params.id
    }).limit(100).exec(function(err, hours) {
        if (err) {
            return res.status(500).json({
                error: 'Cannot list Hour Logs'
            });
        }

        return res.json(200, hours);
    });
};

// Updates an existing hours in the DB.
exports.update = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Hours.findById(req.params.id, function(err, hours) {
        if (err) {
            return handleError(res, err);
        }
        if (!hours) {
            return res.send(404);
        }
        var updated = _.merge(hours, req.body);
        updated.save(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, hours);
        });
    });
};

// Deletes a hours from the DB.
exports.destroy = function(req, res) {
    Hours.findById(req.params.id, function(err, hours) {
        if (err) {
            return handleError(res, err);
        }
        if (!hours) {
            return res.send(404);
        }
        hours.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

exports.getTotalCount = function(req, res) {
    Hours.count({}, function(err, count) {
        console.log('Count: ' + count);
        return res.json(200, {
            logs: count
        });
    });
};

exports.getLogsByMonth = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Hours.find({
        "date": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, hours) {
        return res.json(200, hours);
    });
};

exports.getTotalCountByMonth = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Hours.count({
        "date": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, hours) {
        return res.json(200, {
            logs: hours,
            month: month,
            year: year
        });
    });
}
exports.getHoursByMonth = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Hours.find({
        "date": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, hours) {
        var hw = 0;
        for (var i in hours) {
            var log = hours[i];
            hw += hoursWorked(log.timeIn, log.timeOut);
        }
        return res.json(200, {
            hours: hw,
            month: month,
            year: year
        });
    });
}

exports.getHoursCountByDepartment = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Hours.find({
        "department": req.params.department,
        "date": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, hours) {
        var hw = 0;
        for (var i in hours) {
            var log = hours[i];
            console.log(log);
            hw += hoursWorked(log.timeIn, log.timeOut);
        }
        return res.json(200, {
            hours: hw,
            month: month,
            year: year
        });
    });
};

exports.getHoursByDepartment = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Hours.find({
        "department": req.params.department,
        "date": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, hours) {
        return res.json(200, hours);
    });
};

var hoursWorked = function(i, o) {
    var date1;
    var date2;
    var splitted = i.split(' ')[0].split(':');
    if (i.toLowerCase().indexOf('pm') > -1) {
        date1 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
    } else {
        date1 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
    }
    splitted = o.split(' ')[0].split(':');
    if (o.toLowerCase().indexOf('pm') > -1) {
        date2 = new Date(2000, 1, 1, (parseInt(splitted[0]) + 12), parseInt(splitted[1]));
    } else {
        date2 = new Date(2000, 1, 1, parseInt(splitted[0]), parseInt(splitted[1]));
    }
    var diff = Math.abs(date2.getTime() - date1.getTime());
    var mm = diff / 60000;
    return mm;
};

function handleError(res, err) {
    return res.send(500, err);
}
