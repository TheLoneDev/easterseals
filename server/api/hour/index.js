'use strict';

var express = require('express');
var controller = require('./hour.controller');

var router = express.Router();

router.get('/volunteer/:id', controller.findByVolunteer);
router.get('/department/:department/:month/:year', controller.getHoursByDepartment);
router.get('/department/:department/count/:month/:year', controller.getHoursCountByDepartment);
router.get('/hours/:month/:year', controller.getHoursByMonth);
router.get('/:month/:year', controller.getLogsByMonth);
router.get('/count/:month/:year', controller.getTotalCountByMonth);
router.get('/count', controller.getTotalCount);
router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);


module.exports = router;
