'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var HourSchema = new Schema({
  date: Date,
  timeIn: String,
  timeOut: String,
  location: String,
  department: String,
  volunteer: {
    type: Schema.ObjectId,
    ref: 'Volunteer'
  }
});

HourSchema.statics.load = function(id, cb) {
  this.findOne({ _id: id })
  .exec(cb);
};

module.exports = mongoose.model('Hour', HourSchema);
