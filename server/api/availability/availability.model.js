'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AvailabilitySchema = new Schema({
  day: String,
  tod: String
});

AvailabilitySchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).exec(cb);
};


module.exports = mongoose.model('Availability', AvailabilitySchema);
