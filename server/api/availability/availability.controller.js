'use strict';

var _ = require('lodash');
var Availability = require('./availability.model');

// Get list of availabilitys
exports.index = function(req, res) {
  Availability.find(function (err, availabilitys) {
    if(err) { return handleError(res, err); }
    return res.json(200, availabilitys);
  });
};

// Get a single availability
exports.show = function(req, res) {
  Availability.findById(req.params.id, function (err, availability) {
    if(err) { return handleError(res, err); }
    if(!availability) { return res.send(404); }
    return res.json(availability);
  });
};

// Creates a new availability in the DB.
exports.create = function(req, res) {
  Availability.create(req.body, function(err, availability) {
    if(err) { return handleError(res, err); }
    return res.json(201, availability);
  });
};

// Updates an existing availability in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Availability.findById(req.params.id, function (err, availability) {
    if (err) { return handleError(res, err); }
    if(!availability) { return res.send(404); }
    var updated = _.merge(availability, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, availability);
    });
  });
};

// Deletes a availability from the DB.
exports.destroy = function(req, res) {
  Availability.findById(req.params.id, function (err, availability) {
    if(err) { return handleError(res, err); }
    if(!availability) { return res.send(404); }
    availability.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}