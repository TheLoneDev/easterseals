'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VolunteerSchema = new Schema({
    status: String,
    firstName: String,
    lastName: String,
    fullName: String,
    middleName: String,
    address1: String,
    address2: String,
    city: String,
    state: String,
    zip: Number,
    phoneHome: Number,
    phoneCell: Number,
    email: String,
    dob: Number,
    ssn: Number,
    school: String,
    degree: String,
    employer: String,
    jobTitle: String,
    liability: Boolean,
    volunteeredBefore: Boolean,
    volunteeredWhen: String,
    convicted: Boolean,
    convictedWhen: String,
    referred: Boolean,
    referredWho: String,
    specialAccomadations: Boolean,
    specialAccomadationsWhat: String,
    confidentialityForm: Boolean,
    confidentialityFormDate: String,
    backgroundCheck: Boolean,
    backgroundCheckDate: String,
    created: Date,
    availability: {
        monday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        },
        tuesday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        },
        wednesday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        },
        thursday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        },
        friday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        },
        saturday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        },
        sunday: {
            morning: Boolean,
            afternoon: Boolean,
            evening: Boolean
        }

    },
    locations: [],
    areasOfInterest: [],
    specialSkills: [],
    entries: [{
        type: Schema.ObjectId,
        ref: 'Entry'
    }]

});

VolunteerSchema.statics.load = function(id, cb) {
  this.findOne({ _id: id })
  .exec(cb);
};

module.exports = mongoose.model('Volunteer', VolunteerSchema);
