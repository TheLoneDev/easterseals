'use strict';

var _ = require('lodash');
var Volunteer = require('./volunteer.model');

// Get list of volunteers
exports.index = function(req, res) {
    Volunteer.find(function(err, volunteers) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(200, volunteers);
    });
};

// Get a single volunteer
exports.show = function(req, res) {
    Volunteer.findById(req.params.id, function(err, volunteer) {
        if (err) {
            return handleError(res, err);
        }
        if (!volunteer) {
            return res.send(404);
        }
        return res.json(volunteer);
    });
};

// Creates a new volunteer in the DB.
exports.create = function(req, res) {
    req.body.fullName = req.body.firstName + ' ' + req.body.lastName;
    req.body.created = new Date();
    Volunteer.create(req.body, function(err, volunteer) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, volunteer);
    });
};

// Updates an existing volunteer in the DB.
exports.update = function(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Volunteer.findById(req.params.id, function(err, volunteer) {
        if (err) {
            return handleError(res, err);
        }
        if (!volunteer) {
            return res.send(404);
        }
        var updated = _.merge(volunteer, req.body);
        updated.save(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.json(200, volunteer);
        });
    });
};

// Deletes a volunteer from the DB.
exports.destroy = function(req, res) {
    Volunteer.findById(req.params.id, function(err, volunteer) {
        if (err) {
            return handleError(res, err);
        }
        if (!volunteer) {
            return res.send(404);
        }
        volunteer.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

exports.getVolunteerCount = function(req, res) {
    Volunteer.count({}, function(err, volunteers) {

        return res.json(200, {
            volunteers: volunteers
        });
    });
};

exports.getVolunteersByMonth = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Volunteer.find({
        "created": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, volunteers) {
        return res.json(200, volunteers);
    });
};

exports.getTotalVolunteersByMonth = function(req, res) {
    var month = req.params.month;
    var year = req.params.year;
    var mm = new Date(year, month - 1, 1, 1, 1, 1, 1);
    var mmm = new Date(year, month, 1, 1, 1, 1, 1);
    Volunteer.count({
        "created": {
            "$gte": mm,
            "$lte": mmm
        }
    }, function(err, volunteers) {
        return res.json(200, {
            volunteers: volunteers,
            month: month,
            year: year
        });
    });
}

function handleError(res, err) {
    return res.send(500, err);
}
