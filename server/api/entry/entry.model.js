'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EntrySchema = new Schema({
    date: String,
    timeIn: String,
    timeOut: String,
    location: String,
    volunteer: {
        type: Schema.ObjectId,
        ref: 'Volunteer'
    }
});

EntrySchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('volunteer', 'firstName lastName').exec(cb);
};

module.exports = mongoose.model('Entry', EntrySchema);
