#Easter Seals Volunteer Management App
Easter Seals Volunteer App is an application written in the MEAN stack that allows for easy editing, creation and management of volunteers as well as easy reporting for hours worked by time period and department.

##Installation
The app requires the following to run: 
* Nodejs
* Grunt
* Bower
* MongoDB
* Vagrant (Optional)  
For getting started, run the following: 
```sh
$ npm install
```
```sh
$ bower install
```
This will install all the necessary packages as well as bower packages.

##Running

If you have vagrant install, you can run
```sh 
$ vagrant up
```
Followed by a 
```sh
$ vagrant ssh
$ cd /vagrant
$ grunt serve
```
This will start the app in development mode on port 9000

If you do not have vagrant installed, or do not wish to use vagrant, simply run
```sh
$ grunt serve
```
from the home directory (make sure mongoDB is running)

##Deploying
The app can be deployed either as a web application or as a standalone desktop application. 

Run: 
```sh
$ grunt build
```
Note: If it fails, simply use --force to build, this is often caused by the ChartJS naming. 

###Standalone
In the webkit folder, there will be a package.json and index.html, copy those files to the dist directory and overwrite the existing ones. 

Also under config/express.js edit line 20 to
```
var env = 'production';
```
Then, run
```sh
$ grunt nodewebkit
```
This command will create deployment packages for each version (Windows, OSX, Linux) in the webkitbuilds folder


